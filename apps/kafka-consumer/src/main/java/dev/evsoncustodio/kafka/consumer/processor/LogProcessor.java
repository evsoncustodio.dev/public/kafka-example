package dev.evsoncustodio.kafka.consumer.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogProcessor implements Processor {

  @Override
  public void process(Exchange exchange) throws Exception {
    log.info("Body : {}", exchange.getIn().getBody());
    log.info("Offset : {}", exchange.getIn().getHeader("kafka.OFFSET"));
  }

}
