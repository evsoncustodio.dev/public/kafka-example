package dev.evsoncustodio.kafka.consumer.order;

import java.math.BigDecimal;

import org.apache.camel.builder.RouteBuilder;

import dev.evsoncustodio.kafka.model.Order;

public class OrderProcessRoute extends RouteBuilder {

  @Override
  public void configure() throws Exception {

    from("direct:order-process")
      .routeId("order-process")
      .errorHandler(
        defaultErrorHandler()
          .log("order-process-error-handler")
          .useOriginalMessage()
          .logExhausted(true)
          .logStackTrace(false)
          .logExhaustedMessageHistory(false)
          .maximumRedeliveries(3)
          .redeliveryDelay(1000)
          .backOffMultiplier(2))
      .choice()
        .when(exchange -> exchange.getIn().getBody(Order.class).getTotal().compareTo(BigDecimal.valueOf(500)) < 0)
          .throwException(RuntimeException.class, "Total lower than 500")
      .end()
      .log("Order processed successfully")
      ;
  }
}
