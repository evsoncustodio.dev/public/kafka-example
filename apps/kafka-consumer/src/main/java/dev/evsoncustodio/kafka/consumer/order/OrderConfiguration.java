package dev.evsoncustodio.kafka.consumer.order;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OrderConfiguration {

  @Bean
  public OrderLogRoute orderLogRoute() {
    return new OrderLogRoute();
  }

  @Bean
  public OrderProcessRoute orderProcessRouter() {
    return new OrderProcessRoute();
  }
}
