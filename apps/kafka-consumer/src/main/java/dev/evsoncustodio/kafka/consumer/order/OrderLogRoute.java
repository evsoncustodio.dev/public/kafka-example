package dev.evsoncustodio.kafka.consumer.order;

import org.apache.camel.builder.RouteBuilder;

import dev.evsoncustodio.kafka.model.Order;

public class OrderLogRoute extends RouteBuilder {

  private static final String ORIGINAL_MESSAGE = "ORIGINAL_MESSAGE";
  private static final String ATTEMPTS_HEADER = "Attempts";

  @Override
  public void configure() throws Exception {

    from("kafka:order")
      .routeId("order-log")
      .log("Order received from Kafka : ${body}")
      .log("    on the topic ${headers[kafka.TOPIC]}")
      .log("    on the partition ${headers[kafka.PARTITION]}")
      .log("    with the offset ${headers[kafka.OFFSET]}")
      .log("    with the key ${headers[kafka.KEY]}")
      .setProperty(OrderLogRoute.ORIGINAL_MESSAGE, simple("${body}"))
      .errorHandler(
        deadLetterChannel("kafka:order")
          .onExceptionOccurred(e -> {
            e.getMessage().setBody(e.getProperty(OrderLogRoute.ORIGINAL_MESSAGE));
            e.getMessage().setHeader(OrderLogRoute.ATTEMPTS_HEADER,
              String.valueOf(e.getIn().getHeader(OrderLogRoute.ATTEMPTS_HEADER) == null
                ? 3
                : (e.getIn().getHeader(OrderLogRoute.ATTEMPTS_HEADER, Integer.class) - 1)));
          })
          .maximumRedeliveries(3)
          .redeliveryDelay(1000)
          .backOffMultiplier(2))
      .choice()
        .when(e -> e.getIn().getHeader(OrderLogRoute.ATTEMPTS_HEADER) == null
                || e.getIn().getHeader(OrderLogRoute.ATTEMPTS_HEADER, Integer.class) > 0)
          .unmarshal().json(Order.class)
          .doTry()
            .to("direct:order-process")
          .doCatch(Exception.class)
            .log("Error executing processing")
            .throwException(RuntimeException.class, exceptionMessage().toString())
        .endChoice()
        .otherwise()
          .log("Unable to process the Order : ${body}")
        .end()
      ;
  }
}
