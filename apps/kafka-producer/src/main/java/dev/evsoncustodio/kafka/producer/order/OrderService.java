package dev.evsoncustodio.kafka.producer.order;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.maximeroussy.invitrode.WordGenerator;

import dev.evsoncustodio.kafka.model.Order;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderService {

  private final WordGenerator wordGenerator;
  private final SecureRandom secureRandom;

  public Order generateRandomOrder(int numberOfProducts) {
    return Order.builder()
      .uuid(UUID.randomUUID().toString())
      .products(IntStream.rangeClosed(1, numberOfProducts)
        .mapToObj(i -> wordGenerator.newWord(this.secureRandom.nextInt(10) + 3))
        .collect(Collectors.toList()))
      .total(BigDecimal.valueOf(this.secureRandom.nextDouble() * 1000))
      .build();
  }
}
