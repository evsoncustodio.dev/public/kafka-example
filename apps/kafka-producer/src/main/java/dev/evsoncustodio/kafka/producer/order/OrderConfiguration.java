package dev.evsoncustodio.kafka.producer.order;

import java.security.SecureRandom;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.maximeroussy.invitrode.WordGenerator;

@Configuration
public class OrderConfiguration {

  @Bean
  public WordGenerator wordGenerator() {
    return new WordGenerator();
  }

  @Bean
  public SecureRandom secureRandom() {
    return new SecureRandom();
  }

  @Bean
  public OrderService orderService(WordGenerator wordGenerator, SecureRandom secureRandom) {
    return new OrderService(wordGenerator, secureRandom);
  }

  @Bean
  public OrderSchedulingRoute orderSchedulingRouter(OrderService orderService, SecureRandom secureRandom) {
    return new OrderSchedulingRoute(orderService, secureRandom);
  }
}
