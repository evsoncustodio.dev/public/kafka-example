package dev.evsoncustodio.kafka.producer.order;

import java.security.SecureRandom;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.kafka.KafkaConstants;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class OrderSchedulingRoute extends RouteBuilder {

  private final OrderService orderService;
  private final SecureRandom secureRandom;

  @Override
  public void configure() throws Exception {
    from("scheduler:order-scheduling?delay=3000")
      .routeId("order-scheduling")
      .setBody(exchange -> this.orderService.generateRandomOrder(this.secureRandom.nextInt(2, 5)))
      .setHeader(KafkaConstants.KEY, simple("${body.uuid}"))
      .marshal().json()
      .log("Order produced to   Kafka : ${body}")
      .to("kafka:order")
      ;
  }
}
