package dev.evsoncustodio.kafka.model.deadletterroute;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.Data;

@Data
public class DeadLetterRouteProperties {
  private boolean enabled = true;

  private EndpointProperties from;
  private EndpointProperties to;

  private int delay = 1000;
  private int backOff = 2;
  private int redeliveries = 0;

  private String attemptHeaderName = "Attempts";
  private int attemptInitialValue = 3;
  private String originalBodyProperty = "ORIGINAL_BODY";

  @Data
  public static class EndpointProperties {
    private String uri;
    private Map<String, String> params;

    public String getFormattedParams() {
      return Optional.ofNullable(this.params)
        .map(p -> Optional.ofNullable(p.isEmpty() ? null : p.entrySet().stream())
          .map(s -> "?".concat(s.map(e -> e.getKey()
            .concat("=")
            .concat(e.getValue()))
            .collect(Collectors.joining("&"))))
          .orElse(""))
        .orElse("");
    }

    public String getUriWithParams() {
      return this.uri.concat(this.getFormattedParams());
    }

    public String getRouteId() {
      return this.uri.replace(":", "-");
    }
  }
}
