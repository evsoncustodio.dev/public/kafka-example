package dev.evsoncustodio.kafka.model.camel.route.simple;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.Getter;

@Data
public class SimpleRouteProperties {
  private boolean enabled = true;
  private RedeliveryProperties redelivery = new RedeliveryProperties();
  private ThreadsProperties threads = new ThreadsProperties();
  private DelayProperties delay = new DelayProperties();

  @NotNull
  private EndpointProperties from;

  @NotNull
  private EndpointProperties to;

  public SimpleRouteProperties setDefaultBodyProperty() {
    if (this.from.getBodyProperty() == null || this.from.getBodyProperty().isBlank()) {
      this.from.setBodyProperty(this.from.getRouteId().concat("-body-input"));
    }
    if (this.to.getBodyProperty() == null || this.to.getBodyProperty().isBlank()) {
      this.to.setBodyProperty(this.from.getRouteId().concat("-body-output"));
    }
    return this;
  }

  @Data
  public static class RedeliveryProperties {
    private boolean logExhausted = true;
    private boolean logStackTrace = false;
    private boolean logExhaustedMessageHistory = false;
    private int delay = 1000;
    private int backOff = 2;
    private int maximum = 3;
    private String attemptHeader = "Attempts";
    private int attemptInitialValue = 3;
    private EndpointProperties deadLetter;
  }

  @Data
  public static class ThreadsProperties {
    private int poolSize = 0;
    private int maxPoolSize = 20;
    private int maxQueueSize = 1000;
    private int keepAliveTime = 60;
    private boolean allowCoreThreadTimeOut = true;
  }

  @Data
  public static class DelayProperties {
    private long millis = 0;
    private boolean async = true;
  }

  @Data
  public static class EndpointProperties {

    @NotBlank
    private String uri;
    private Map<String, String> params;
    private String bodyProperty;
    private BodyFormat bodyFormat = BodyFormat.NONE;
    private String bodyType;

    public String getFormattedParams() {
      return Optional.ofNullable(this.params)
        .map(p -> Optional.ofNullable(p.isEmpty() ? null : p.entrySet().stream())
          .map(s -> "?".concat(s.map(e -> e.getKey()
            .concat("=")
            .concat(e.getValue()))
            .collect(Collectors.joining("&"))))
          .orElse(""))
        .orElse("");
    }

    public String getUriWithParams() {
      return this.uri.concat(this.getFormattedParams());
    }

    public String getRouteId() {
      return this.uri.replace(":", "-");
    }

    public String getErrorHandlerName() {
      return this.getRouteId().concat("-error-handler");
    }
  }

  @Getter
  public enum BodyFormat {
    NONE,
    MARSHAL,
    UNMARSHAL
  }
}
