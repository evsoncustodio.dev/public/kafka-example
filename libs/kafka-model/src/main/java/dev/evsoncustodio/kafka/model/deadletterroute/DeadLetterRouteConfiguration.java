package dev.evsoncustodio.kafka.model.deadletterroute;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.CamelContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class DeadLetterRouteConfiguration {

  @Bean
  @ConfigurationProperties(prefix = "dead-letter-routes")
  public List<DeadLetterRouteProperties> deadLetterRouteProperties() {
    return new ArrayList<>();
  }

  @Bean
  public List<DeadLetterRouteAttemptProcessor> deadLetterRouteAttemptProcessors(List<DeadLetterRouteProperties> deadLetterRouteProperties) {
    return deadLetterRouteProperties.stream()
      .filter(DeadLetterRouteProperties::isEnabled)
      .map(DeadLetterRouteAttemptProcessor::new)
      .collect(Collectors.toList());
  }

  @Bean
  public List<DeadLetterRoute> deadLetterRoutes(CamelContext camelContext, List<DeadLetterRouteAttemptProcessor> deadLetterRouteAttemptProcessors) {
    return deadLetterRouteAttemptProcessors.stream()
      .map(p -> {
        DeadLetterRoute deadLetterRoute = new DeadLetterRoute(p.getDeadLetterRouteProperties(), p);
        try {
          camelContext.addRoutes(deadLetterRoute);
        } catch (Exception e) {
          log.info("Error loading route: {}", deadLetterRoute);
        }
        return deadLetterRoute;
      })
      .collect(Collectors.toList());
  }
}
