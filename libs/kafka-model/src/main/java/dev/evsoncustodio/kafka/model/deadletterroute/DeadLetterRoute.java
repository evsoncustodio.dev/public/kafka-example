package dev.evsoncustodio.kafka.model.deadletterroute;

import org.apache.camel.builder.RouteBuilder;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class DeadLetterRoute extends RouteBuilder {

  private static final String EXCEPTION_MESSAGE = "An exception occurred in ${variable.toRouteId} : ${exception.message}";
  private static final String ATTEMPTS_EXPIRE_MESSAGE = "Unable to process the message : ${body}";

  private final DeadLetterRouteProperties deadLetterRouteProperties;
  private final DeadLetterRouteAttemptProcessor deadLetterRouteAttemptProcessor;

  @Override
  public void configure() throws Exception {
    from(this.deadLetterRouteProperties.getFrom().getUriWithParams())
      .routeId(this.deadLetterRouteProperties.getFrom().getRouteId())
      .setProperty(this.deadLetterRouteProperties.getOriginalBodyProperty(), simple("${body}"))
      .errorHandler(
        deadLetterChannel(this.deadLetterRouteProperties.getFrom().getUriWithParams())
          .onExceptionOccurred(this.deadLetterRouteAttemptProcessor)
          .maximumRedeliveries(this.deadLetterRouteProperties.getRedeliveries())
          .redeliveryDelay(this.deadLetterRouteProperties.getDelay())
          .backOffMultiplier(this.deadLetterRouteProperties.getBackOff()))
      .choice()
        .when(e -> e.getIn().getHeader(this.deadLetterRouteProperties.getAttemptHeaderName()) == null
                || e.getIn().getHeader(this.deadLetterRouteProperties.getAttemptHeaderName(), Integer.class) > 0)
          .unmarshal().json()
          .doTry()
            .to(this.deadLetterRouteProperties.getTo().getUriWithParams())
          .doCatch(Exception.class)
            .setVariable("toRouteId", this.deadLetterRouteProperties.getTo()::getRouteId)
            .log(DeadLetterRoute.EXCEPTION_MESSAGE)
            .throwException(RuntimeException.class, DeadLetterRoute.EXCEPTION_MESSAGE)
          .endChoice()
          .otherwise()
            .log(DeadLetterRoute.ATTEMPTS_EXPIRE_MESSAGE)
          .end()
        ;
  }
}
