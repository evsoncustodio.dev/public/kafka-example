package dev.evsoncustodio.kafka.model.deadletterroute;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class DeadLetterRouteAttemptProcessor implements Processor {

  private final DeadLetterRouteProperties deadLetterRouteProperties;

  @Override
  public void process(Exchange exchange) throws Exception {
    exchange.getMessage().setBody(exchange.getProperty(this.deadLetterRouteProperties.getOriginalBodyProperty()));
    exchange.getMessage().setHeader(this.deadLetterRouteProperties.getAttemptHeaderName(),
      String.valueOf(exchange.getIn().getHeader(this.deadLetterRouteProperties.getAttemptHeaderName()) == null
        ? this.deadLetterRouteProperties.getAttemptInitialValue()
        : (exchange.getIn().getHeader(this.deadLetterRouteProperties.getAttemptHeaderName(), Integer.class) - 1)));
  }
}
