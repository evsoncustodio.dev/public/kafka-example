package dev.evsoncustodio.kafka.model.camel.route.simple;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.ProcessorDefinition;

import dev.evsoncustodio.kafka.model.camel.route.simple.SimpleRouteProperties.DelayProperties;
import dev.evsoncustodio.kafka.model.camel.route.simple.SimpleRouteProperties.EndpointProperties;
import dev.evsoncustodio.kafka.model.camel.route.simple.SimpleRouteProperties.RedeliveryProperties;
import dev.evsoncustodio.kafka.model.camel.route.simple.SimpleRouteProperties.ThreadsProperties;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class SimpleRoute extends RouteBuilder {

  private static final String ATTEMPTS_EXPIRE_MESSAGE = "Unable to process the message : ${body}";
  private static final String EXCHANGE_HEADERS = "Exchange Headers: ${headers}";
  private static final String BODY_RECEIVED = "Body Received: ${body}";

  private final SimpleRouteProperties simpleRouteProperties;

  @Override
  public void configure() throws Exception {
    RedeliveryProperties redelivery = this.simpleRouteProperties.getRedelivery();
    ThreadsProperties threads = this.simpleRouteProperties.getThreads();
    DelayProperties delay = this.simpleRouteProperties.getDelay();
    EndpointProperties fromEndpoint = this.simpleRouteProperties.getFrom();
    EndpointProperties toEndpoint = this.simpleRouteProperties.getTo();

    ProcessorDefinition<?> definition = from(fromEndpoint.getUriWithParams())
      .routeId(fromEndpoint.getRouteId())
      .errorHandler(
        (redelivery.getDeadLetter() != null
        ? deadLetterChannel(redelivery.getDeadLetter().getUriWithParams())
          .onExceptionOccurred(exchange -> {
            exchange.getMessage().setBody(exchange.getProperty(fromEndpoint.getBodyProperty()));
            if (exchange.getIn().getHeader(redelivery.getAttemptHeader()) == null) {
              exchange.getMessage().setHeader(redelivery.getAttemptHeader(),
                String.valueOf(redelivery.getAttemptInitialValue()));
            }
            else {
              exchange.getMessage().setHeader(redelivery.getAttemptHeader(),
                String.valueOf(exchange.getIn().getHeader(redelivery.getAttemptHeader(), Integer.class) - 1));
            }
          })
        : defaultErrorHandler()
          .useOriginalMessage())
          .log(fromEndpoint.getErrorHandlerName())
          .logHandled(redelivery.isLogExhausted())
          .logExhausted(redelivery.isLogExhausted())
          .logStackTrace(redelivery.isLogStackTrace())
          .logExhaustedMessageHistory(redelivery.isLogExhaustedMessageHistory())
          .maximumRedeliveries(redelivery.getMaximum())
          .redeliveryDelay(redelivery.getDelay())
          .backOffMultiplier(redelivery.getBackOff()));

      definition = this.threadsDefinition(definition, threads);
      definition = this.delayDefinition(definition, delay)
        .log(SimpleRoute.EXCHANGE_HEADERS)
        .log(SimpleRoute.BODY_RECEIVED)
        .choice()
        .when(e -> e.getIn().getHeader(redelivery.getAttemptHeader()) == null
                || e.getIn().getHeader(redelivery.getAttemptHeader(), Integer.class) > 0);

      definition = definition.setProperty(fromEndpoint.getBodyProperty(), body());
      definition = this.bodyFormatDefinition(definition, fromEndpoint)
        .to(toEndpoint.getUriWithParams());
      definition = this.bodyFormatDefinition(definition, toEndpoint);
      definition = definition.setProperty(toEndpoint.getBodyProperty(), body());

      definition.endChoice()
      .otherwise()
        .log(SimpleRoute.ATTEMPTS_EXPIRE_MESSAGE)
      .end();
  }

  protected <T extends ProcessorDefinition<T>> ProcessorDefinition<?> threadsDefinition(ProcessorDefinition<T> definition,
    SimpleRouteProperties.ThreadsProperties threadsProperties) {
    if (threadsProperties.getPoolSize() <= 0) {
      return definition;
    }
    return definition
      .threads()
        .poolSize(threadsProperties.getPoolSize())
        .maxPoolSize(threadsProperties.getMaxPoolSize())
        .maxQueueSize(threadsProperties.getMaxQueueSize())
        .keepAliveTime(threadsProperties.getKeepAliveTime())
        .allowCoreThreadTimeOut(threadsProperties.isAllowCoreThreadTimeOut());
  }

  protected <T extends ProcessorDefinition<T>> ProcessorDefinition<?> delayDefinition(ProcessorDefinition<T> definition,
    SimpleRouteProperties.DelayProperties delayProperties) {
    if (delayProperties.getMillis() <= 0) {
      return definition;
    }
    return delayProperties.isAsync()
      ? definition.delay(delayProperties.getMillis()).asyncDelayed().end()
      : definition.delay(delayProperties.getMillis()).syncDelayed().end();
  }

  protected <T extends ProcessorDefinition<T>> ProcessorDefinition<T> bodyFormatDefinition(ProcessorDefinition<T> definition,
    SimpleRouteProperties.EndpointProperties endpointProperties) throws ClassNotFoundException {
    if (SimpleRouteProperties.BodyFormat.MARSHAL.equals(endpointProperties.getBodyFormat())) {
      definition.marshal().json();
    }
    else if (SimpleRouteProperties.BodyFormat.UNMARSHAL.equals(endpointProperties.getBodyFormat())) {
      definition.unmarshal().json(Class.forName(endpointProperties.getBodyType()));
    }
    return definition;
  }
}
