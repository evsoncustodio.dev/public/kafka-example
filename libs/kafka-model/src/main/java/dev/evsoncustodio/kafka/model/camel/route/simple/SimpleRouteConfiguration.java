package dev.evsoncustodio.kafka.model.camel.route.simple;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.camel.CamelContext;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class SimpleRouteConfiguration {

  @Bean
  @ConfigurationProperties(prefix = "simple-routes")
  public List<SimpleRouteProperties> simpleRouteProperties() {
    return new ArrayList<>();
  }

  @Bean
  public List<SimpleRoute> simpleRoutes(CamelContext camelContext, List<SimpleRouteProperties> simpleRouteProperties) {
    return simpleRouteProperties.stream()
      .filter(SimpleRouteProperties::isEnabled)
      .map(p -> {
        log.info("Simple Route Property: {}", p);
        SimpleRoute simpleRoute = new SimpleRoute(p.setDefaultBodyProperty());
        try {
          camelContext.addRoutes(simpleRoute);
        } catch (Exception e) {
          log.info("Error loading route: {}", e);
        }
        return simpleRoute;
      })
      .collect(Collectors.toList());
  }
}
